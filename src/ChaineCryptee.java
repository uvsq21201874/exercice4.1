
public class ChaineCryptee {
	
	
	private String cryptee;
	private int decalage;
	
	private ChaineCryptee(String enclair, int decalage){
		if(enclair!=null){
			String cryptee="";
			for(char c : enclair.toCharArray()){
				cryptee+=decaleCaractere(c,decalage); //
			}
				this.cryptee=cryptee;
			}
			else{
				System.out.println("Impossible de chiffrer une chaine vide");
				return ;
			}
		
		this.decalage=decalage;
	}
	
	//constructeur par défaut pour faire la fonction FromCrypted()
	private ChaineCryptee(){
		
	}
	
	public String decrypte(){
		if(cryptee!=null){
			String enclair="";
			for(char c : cryptee.toCharArray()){
				enclair+=decaleCaractere(c,-decalage);
			}
			return enclair;
		}
		else{
			return cryptee;
		}
	}
	
	
	
	private static char decaleCaractere(char c, int decalage){
		return (c <'A' || c > 'Z')? c : (char)(((c-'A'+decalage)%26)+'A');//on fait c-'A' pour déchiffrer
	}
	
	
	public String crypte(){
		return cryptee;
	}
	
	/** 
	 * 
	 * @return La méthode crypte chiffre la chaine de caractère à l'aide de la méthode decaleCaractere(char c,String decalage)
	 * On exécute le chiffrement seulement si la chaine enclair est non vide.
	 */
	//public String crypte(){
	//	if(enclair!=null){
	//		String enclair=this.enclair;
	//		String encrypte="";
	//		for(char c : enclair.toCharArray()){ //un char est affecté à c à chaque tour jusqu'a qu'il soit null
	//			encrypte+=decaleCaractere(c,decalage); //
	//		}
	//		return encrypte;
	//	}
	//	else{
	//		System.out.println("Impossible de chiffrer une chaine vide");
	//		return enclair;
	//	}
	//}
	
	public static ChaineCryptee FromCrypted(String cryptee, int decalage){
		 ChaineCryptee n = new ChaineCryptee();
		 n.cryptee= cryptee;
		 n.decalage=decalage;
		 return n;
	}
	
	/*
	 * 1) il faut crypter la chaine
	 * 2) il faut la stocker
	 * 3) encryptedString es = FromCrypted(crypted,shift)
	 */
	
	public static ChaineCryptee FromDecrypted(String enclair, int decalage){
		return new ChaineCryptee(enclair,decalage);
				
	}
	
}
